from os import path, remove
from sys import exit
from datetime import datetime
from othello.planche import Planche
from othello.joueur import JoueurOrdinateur, JoueurHumain

class Partie:
    def __init__(self, nom_fichier=None, menu=False):
        """
        Méthode d'initialisation d'une partie. On initialise 4 membres:
        - planche: contient la planche de la partie, celui-ci contenant le dictionnaire de pièces.
        - couleur_joueur_courant: le joueur à qui c'est le tour de jouer.
        - tour_precedent_passe: un booléen représentant si le joueur précédent a passé son tour parce qu'il
           n'avait aucun coup disponible.
        - deux_tours_passes: un booléen représentant si deux tours ont été passés de suite, auquel cas la partie
           devra se terminer.
        - coups_possibles : une liste de tous les coups possibles en fonction de l'état actuel de la planche,
           initialement vide.

        On initialise ensuite les joueurs selon la paramètre nom_fichier. Si l'utilisateur a précisé un nom_fichier,
        on fait appel à la méthode self.charger() pour charger la partie à partir d'un fichier. Sinon, on fait appel
        à self.initialiser_joueurs(), qui va demander à l'utilisateur quels sont les types de joueurs qu'il désire.
        """
        self.planche = Planche()

        self.partie_en_cours = 'partie_en_cours.txt' # Attribut pour l'enregistrement et chargement des parties en cours.

        self.couleur_joueur_courant = "noir"

        self.tour_precedent_passe = False

        self.deux_tours_passes = False

        self.coups_possibles = []

        if menu:
            self.menu_douverture()
        elif nom_fichier is not None:
            self.charger(nom_fichier)
        else:
            self.initialiser_joueurs()

    def initialiser_joueurs(self):
        """
        On initialise ici trois attributs : joueur_noir, joueur_blanc et joueur_courant (initialisé à joueur_noir).

        Pour créer les objets joueur, faites appel à demander_type_joueur()
        """
        self.joueur_noir, self.joueur_blanc = self.demander_type_joueur('noir'), self.demander_type_joueur('blanc')

        # Le joueur noir est le premier joueur à jouer.
        self.joueur_courant = self.joueur_noir

    def demander_type_joueur(self, couleur):
        """
        Demande à l'usager quel type de joueur ('Humain' ou 'Ordinateur') il désire pour le joueur de la couleur.

        Tant que l'entrée n'est pas valide, on continue de demander à l'utilisateur.

        Faites appel à self.creer_joueur() pour créer le joueur lorsque vous aurez le type.

        Args:
            couleur: La couleur pour laquelle on veut le type de joueur.

        Returns:
            Un objet Joueur, de type JoueurHumain si l'usager a entré 'Humain', JoueurOrdinateur s'il a entré
            'Ordinateur'.
        """
        type_joueur = input(f'Quel type de joueur souhaitez-vous utiliser pour l\'équipe des {couleur}s '
                            f': \'\033[04mHumain\033[30m\' ou \'\033[04mOrdinateur\033[30m\' ? ')

        # Tant que l'utilisateur n'entre pas une donnée valide, on retourne une erreur et on redemande la question.
        while (type_joueur != 'Humain') and (type_joueur != 'Ordinateur'):

            print(f'\033[31mErreur : \'{type_joueur}\' n\'est pas une entrée valide.\033[30m')

            type_joueur = input(f'Quel type de joueur souhaitez-vous utiliser pour l\'équipe des {couleur}s '
                                ': \'\033[04mHumain\033[30m\' ou \'\033[04mOrdinateur\033[30m\' ? ')

        # Pour le joueur Humain, on demande son nom afin de pouvoir l'inscrire dans l'historique des parties.
        # Pour le joueur Ordinateur, on demande à l'utilisateur quel niveau de difficulté il souhaite.
        if type_joueur == 'Humain':
            nom_joueur = input('Quel est votre nom de joueur ? ')
            while '/' in nom_joueur:
                print('\033[31mErreur : Pas de caractères spéciaux.\033[30m')
                nom_joueur = input('Quel est votre nom de joueur ? ')
            return self.creer_joueur(type_joueur, couleur, nom = nom_joueur)
        else :
            niveau_joueur = input(f'Quel niveau de difficulté souhaitez-vous pour le joueur ordinateur {couleur} : '
                                  '\'\033[04mFacile\033[30m\', \'\033[04mNormal\033[30m\' ? ')

            while (niveau_joueur != 'Facile') and (niveau_joueur != 'Normal'):
                print(f'\033[31mErreur : \'{type_joueur}\' n\'est pas une entrée valide.\033[30m')
                niveau_joueur = input(f'Quel niveau de difficulté souhaitez-vous pour le joueur ordinateur {couleur} : '
                                      '\'\033[04mFacile\033[30m\', \'\033[04mNormal\033[30m\' ? ')

            return self.creer_joueur(type_joueur, couleur, niveau = niveau_joueur)

    def creer_joueur(self, type, couleur, nom = '', niveau = 'Facile'):
        """
        Crée l'objet Joueur approprié, selon le type passé en paramètre.

        Pour créer les objets, vous n'avez qu'à faire appel à leurs constructeurs, c'est-à-dire à
        JoueurHumain(couleur), par exemple.

        Args:
            type: le type de joueur, "Ordinateur" ou "Humain"
            couleur: la couleur du pion joué par le jouer, "blanc" ou "noir"
            nom: pour les joueurs humains
            niveau: pour les joueurs ordinateur

        Returns:
            Un objet JoueurHumain si le type est "Humain", JoueurOrdinateur sinon
        """
        return (JoueurOrdinateur(couleur, niveau), JoueurHumain(couleur, nom))[type == 'Humain']

    def valider_position_coup(self, position_coup):
        """
        Vérifie la validité de la position désirée pour le coup. On retourne un message d'erreur approprié pour
        chacune des trois situations suivantes :

        1) Le coup tenté ne représente pas une position valide de la planche de jeu.

        2) Une pièce se trouve déjà à la position souhaitée.

        3) Le coup ne fait pas partie de la liste des coups valides.

        ATTENTION: Utilisez les méthodes et attributs de self.planche ainsi que la liste self.coups_possibles pour
                   connaître les informations nécessaires.
        ATTENTION: Bien que cette méthode valide plusieurs choses, les méthodes programmées dans la planche vous
                   simplifieront la tâche!

        Args:
            position_coup: La position du coup à valider.

        Returns:
            Un couple où le premier élément représente la validité de la position (True ou False), et le
            deuxième élément est un éventuel message d'erreur.
        """
        # On initialise nos valeurs de retour.
        position_valide : bool = False
        message : str = None

        if not self.planche.position_valide(position_coup):
            message = '\033[31mErreur : La position de votre coup n\'est pas à l\'intérieur des limites de la planche.\033[30m'

        elif not self.planche.get_piece(position_coup) == None:
            message = '\033[31mErreur : Une pièce se trouve déjà à cette position.\033[30m'

        elif position_coup not in self.coups_possibles:
            message = '\033[31mErreur : Ce coup ne fait pas partie des possibilités que vous avez.\033[30m'

        else: position_valide = True # Si la position du coup est possible, on retourne True.

        return (position_valide, message)

    def tour(self):
        """
        Cette méthode simule le tour d'un joueur, et doit effectuer les actions suivantes:
        - Demander la position du coup au joueur courant. Tant que la position n'est pas validée, on continue de
          demander. Si la position est invalide, on affiche le message d'erreur correspondant. Pour demander la
          position, faites appel à la fonction choisir_coup de l'attribut self.joueur_courant, à laquelle vous
          devez passer la liste de coups possibles. Pour valider le coup retourné, pensez à la méthode de validation
          de coup que vous avez déjà à implémenter.
        - Jouer le coup sur la planche de jeu, avec la bonne couleur.
        - Si le résultat du coup est "erreur", afficher un message d'erreur.

        ***Vous disposez d'une méthode pour demander le coup à l'usager dans cette classe et la classe planche
        possède à son tour une méthode pour jouer un coup, utilisez-les !***
        """
        # Si le joueur est un 'Humain'
        if self.joueur_courant.obtenir_type_joueur() == 'Humain':

            # On détermine le coup du joueur courant.
            position_coup = self.joueur_courant.choisir_coup(self.coups_possibles)

            # On s'assure de la validité du coup.
            while not self.valider_position_coup(position_coup)[0]:

                print(self.valider_position_coup(position_coup)[1])

                position_coup = self.joueur_courant.choisir_coup(self.coups_possibles)

        else :
            # Le paramètre 'planche' est optionnel pour le JoueurOrdinateur en niveau 'facile'
            position_coup = self.joueur_courant.choisir_coup(self.coups_possibles, self.planche)

        # On joue finalement le coup. Si il y a une erreur, on affiche celle-ci.
        if self.planche.jouer_coup(position_coup, self.couleur_joueur_courant) == 'erreur':
            print('\033[31mErreur : Il y a une erreur dans le coup jouer sur la planche...\033[30m')

    def passer_tour(self):
        """
        Affiche un message indiquant que le joueur de la couleur courante ne peut jouer avec l'état actuel de la
        planche et qu'il doit donc passer son tour.
        """
        print(f'\033[33mLe joueur de l\'équipe des {self.couleur_joueur_courant}s n\'a pas la possibilité de jouer'
              ' et doit donc passer son tour.\033[30m')

    def partie_terminee(self):
        """
        Détermine si la partie est terminée, Une partie est terminée si toutes les cases de la planche sont remplies
        ou si deux tours consécutifs ont été passés (pensez à l'attribut self.deux_tours_passes).
        """
        # Pour déterminer si toutes les cases de la planche sont remplies..
        # On regarde si il y a 64 pieces dans la planche.
        return (False, True)[(self.deux_tours_passes == True) or (len(self.planche.cases) == 64)]

    def determiner_gagnant(self):
        """
        Détermine le gagnant de la partie. Le gagnant est simplement la couleur pour laquelle il y a le plus de
        pions sur la planche de jeu.

        Affichez un message indiquant la couleur gagnante ainsi que le nombre de pièces de sa couleur ou encore
        un message annonçant un match nul, le cas échéant.
        """
        points_blanc, points_noir = 0, 0

        for piece in self.planche.cases.values():
            # On calcul dans notre planche chacune des pièces correspondant à la couleur du joueur.
            if piece.est_blanc(): points_blanc += 1
            else: points_noir += 1

        if points_blanc != points_noir:
            gagnant = (('blanc', points_blanc), ('noir', points_noir))[points_blanc < points_noir]
            print(f'\033[34mLe gagnant de cette partie est le joueur de l\'équipe des {gagnant[0]}s '
                  f'avec un total de {gagnant[1]} points.\033[30m')

            # On retourne les informations concernant le gagnant pour différentes méthodes ajoutés au TP.
            return gagnant
        else:
            print(f'\033[34mMatch nul : {points_blanc}/{points_noir} ! Bien joué aux deux équipes.\033[30m')
            return 'null'

    def jouer(self):
        """
        Démarre une partie. Tant que la partie n'est pas terminée, on fait les choses suivantes :

        1) On affiche la planche de jeu ainsi qu'un message indiquant à quelle couleur c'est le tour de jouer.
           Pour afficher une planche, faites appel à print(self.planche)

        2) On détermine les coups possibles pour le joueur actuel. Pensez à utiliser une fonction que vous avez à
           implémenter pour Planche, et à entreposer les coups possibles dans un attribut approprié de la partie.

        3) Faire appel à tour() ou à passer_tour(), en fonction du nombre de coups disponibles pour le joueur actuel.
           Mettez aussi à jour les attributs self.tour_precedent_passe et self.deux_tours_passes.

        4) Effectuer le changement de joueur. Modifiez à la fois les attributs self.joueur_courant et
           self.couleur_joueur_courant.

        5) Lorsque la partie est terminée, afficher un message mentionnant le résultat de la partie. Vous avez une
           fonction à implémenter que vous pourriez tout simplement appeler.
        """
        while not self.partie_terminee():

            # On détermine les coups possibles.
            self.coups_possibles.clear()
            self.coups_possibles = self.planche.lister_coups_possibles_de_couleur(self.couleur_joueur_courant)

            # Impression de points de références pour les coups possibles du joueur.
            for coup in self.coups_possibles: self.planche.cases[coup] = '\033[31m\u0387\033[30m'

            print(self.planche)
            print(f'\033[34mC\'est au tour de l\'équipe des {self.couleur_joueur_courant}s de jouer.\033[30m')

            # On s'assure de supprimer les références avant de jouer le tour.
            for coup in self.coups_possibles: del self.planche.cases[coup]

            # Selon les coups possibles, on joue le tour ou on passe le tour.
            if self.coups_possibles:
                self.tour()
                if self.tour_precedent_passe : self.tour_precedent_passe = False
            else :
                self.passer_tour()
                # On met à jour les attributs
                if self.tour_precedent_passe : self.deux_tours_passes = True
                else : self.tour_precedent_passe = True

            # Changement de joueur
            if self.couleur_joueur_courant == 'noir':
                self.joueur_courant = self.joueur_blanc
                self.couleur_joueur_courant = 'blanc'
            else:
                self.joueur_courant = self.joueur_noir
                self.couleur_joueur_courant = 'noir'

            # On sauvegarde la partie seulement si il y a au moins un joueur 'humain'.
            if (self.joueur_blanc.obtenir_type_joueur() == 'Humain') or (self.joueur_noir.obtenir_type_joueur() == 'Humain'):
                self.sauvegarder(self.partie_en_cours)

        # Quand la partie est terminé.
        print(self.planche)
        gagnant = self.determiner_gagnant()

        # On supprime le fichier de partie en cours.
        if path.exists(self.partie_en_cours): remove(self.partie_en_cours)

        # Si un 'humain' gagne une partie, on enregistre celle-ci dans l'historique
        if (gagnant != 'null') and ((self.joueur_noir.obtenir_type_joueur() == 'Humain' and gagnant[0] == 'noir') or
                                    (self.joueur_blanc.obtenir_type_joueur() == 'Humain' and gagnant[0] == 'blanc')):
            self.enregistrer_historique(gagnant)

        return gagnant[0] # Le return est seulement pertinant pour les tests de AI.

    def sauvegarder(self, nom_fichier):
        """
        Sauvegarde une partie dans un fichier. Le fichier condiendra:
        - Une ligne indiquant la couleur du joueur courant.
        - Une ligne contenant True ou False, si le tour précédent a été passé.
        - Une ligne contenant True ou False, si les deux derniers tours ont été passés.
        - Une ligne contenant le type du joueur blanc.
        - Une ligne contenant le type du joueur noir.
        - Le reste des lignes correspondant à la planche. Voir la méthode convertir_en_chaine de la planche
         pour le format.

        ATTENTION : L'ORDRE DES PARAMÈTRES SAUVEGARDÉS EST OBLIGATOIRE À RESPECTER.
                    Des tests automatiques seront roulés lors de la correction et ils prennent pour acquis que le
                    format plus haut est respecté. Vous perdrez des points si vous dérogez du format.

        Args:
            nom_fichier: Le nom du fichier où sauvegarder, un string.
        """
        # Le programme enregistre la partie à chaque fin de tour à l'aide de cet attribut.
        self.partie_en_cours = nom_fichier

        sauvegarder_fichier = open(nom_fichier, 'w', encoding='UTF-8')

        # Cette section permet l'utilisation normal de la méthode 'sauvegarder' comme énoncé dans le TP.
        if nom_fichier != 'partie_en_cours.txt':

            lignes = [
                self.couleur_joueur_courant + '\n',
                f'{self.tour_precedent_passe}\n',
                f'{self.deux_tours_passes}\n',
                self.joueur_blanc.obtenir_type_joueur() + '\n',
                self.joueur_noir.obtenir_type_joueur() + '\n',
                self.planche.convertir_en_chaine() + '\n'
            ]

        # Cette section permet d'enregistrer la partie en cours afin de reprendre quand on veut.
        else:

            # On ajoute aussi l'utilisation de paramètres nom(JoueurHumain) et niveau(JoueurOrdinateur) dans nos parties.
            nom_joueurs, niveau_joueurs = '', ''
            if self.joueur_blanc.obtenir_type_joueur() == 'Humain': nom_joueurs +=  self.joueur_blanc.nom_joueur
            else: niveau_joueurs += self.joueur_blanc.niveau
            nom_joueurs += '/'
            niveau_joueurs += '/'
            if self.joueur_noir.obtenir_type_joueur() == 'Humain': nom_joueurs += self.joueur_noir.nom_joueur
            else: niveau_joueurs += self.joueur_noir.niveau

            lignes = [
                nom_joueurs + '\n', # '{joueur_blanc}/{joueur_noir}'
                niveau_joueurs + '\n', # '{joueur_blanc}/{joueur_noir}'
                self.couleur_joueur_courant + '\n',
                f'{self.tour_precedent_passe}\n',
                f'{self.deux_tours_passes}\n',
                self.joueur_blanc.obtenir_type_joueur() + '\n',
                self.joueur_noir.obtenir_type_joueur() + '\n',
                self.planche.convertir_en_chaine() + '\n'
            ]

        sauvegarder_fichier.writelines(lignes)
        sauvegarder_fichier.close()

    def charger(self, nom_fichier):
        """
        Charge une partie dans à partir d'un fichier. Le fichier a le même format que la méthode de sauvegarde.

        Args:
            nom_fichier: Le nom du fichier à charger, un string.
        """
        fichier_charger = open(nom_fichier, 'r')

        # Cette section permet l'utilisation normal de la méthode 'charger' comme énoncé dans le TP.
        if nom_fichier != 'partie_en_cours.txt':

            lignes = fichier_charger.read().split('\n', 5)

            self.couleur_joueur_courant = lignes[0]
            self.tour_precedent_passe = lignes[1] == 'True'
            self.deux_tours_passes = lignes[2] == 'True'

            self.joueur_blanc = self.creer_joueur(lignes[3], 'blanc')
            self.joueur_noir = self.creer_joueur(lignes[4], 'noir')

            # On détermine le joueur courant.
            if self.couleur_joueur_courant == 'blanc': self.joueur_courant = self.joueur_blanc
            else: self.joueur_courant = self.joueur_noir

            self.planche.charger_dune_chaine(lignes[5])

        # Cette section permet de charger la partie en cours.
        else:

            lignes = fichier_charger.read().split('\n', 7)

            nom_joueurs = lignes[0].split('/')
            niveau_joueurs = lignes[1].split('/')

            self.couleur_joueur_courant = lignes[2]
            self.tour_precedent_passe = lignes[3] == 'True'
            self.deux_tours_passes = lignes[4] == 'True'

            if lignes[5] == 'Humain':
                self.joueur_blanc = self.creer_joueur(lignes[5], 'blanc', nom = nom_joueurs[0])
            else:
                self.joueur_blanc = self.creer_joueur(lignes[5], 'blanc', niveau = niveau_joueurs[0])

            if lignes[6] == 'Humain':
                self.joueur_noir = self.creer_joueur(lignes[6], 'noir', nom = nom_joueurs[1])
            else:
                self.joueur_noir = self.creer_joueur(lignes[6], 'noir', niveau = niveau_joueurs[1])

            # On détermine le joueur courant.
            if self.couleur_joueur_courant == 'blanc':
                self.joueur_courant = self.joueur_blanc
            else:
                self.joueur_courant = self.joueur_noir

            self.planche.charger_dune_chaine(lignes[7])

    def afficher_historique(self, nombre_parties):
        """
        On affiche les dernière parties (nombre déterminé par nombre_parties) gagnées par un joueur humain. On peut
        aussi afficher le top 5 des meilleurs joueurs si le paramètre afficher_top est True.

        Args:
            nombre_parties: Nombre entier qui nous dit combien de parties enregistrer dans notre fichier historique.
            afficher_top: Booléen qui détermine si il faut afficher les top 5 des meilleurs joueurs.
        """
        with open('historique.txt', 'r', encoding='UTF-8') as contenu_fichier:
            historique = []
            while True:
                ajout = contenu_fichier.readline().strip().split('/')
                if ajout == ['']:
                    break
                else:
                    historique.append(ajout)

        while True:
            if len(historique) > nombre_parties:
                del historique[0]
            else:
                break
        print()
        if nombre_parties > 1:
            print("Historique des {} dernières parties\n".format(nombre_parties))
        elif nombre_parties == 1:
            print("Historique de la dernière partie")
        print("|{:^20}|{:^9}|{:^8}|{:^6}|{:^6}|{:^8}|".format(' Nom', 'Couleur', 'Points', 'Jour', 'Mois', 'Année'))
        print('{0:->64}'.format(''))
        for element in historique:
            print("| {:<19}|{:^9}|{:^8}|{:^6}|{:^6}|{:^8}|".format(element[0], element[1], element[2], element[3],
                                                                   element[4], element[5]))

    def enregistrer_historique(self, gagnant):
        """
        À la fin de chaque partie gagner par un joueur humain, on enregistre les informations de celle-ci dans un
        fichier qui contient l'historique de toutes les parties.

        Args:
            gagnant: Tuple qui nous retourne les informations sur le gagnant de la partie.
        """
        now = datetime.now()

        # On récupère le contenu du fichier historique.
        with open('historique.txt', 'r', encoding='UTF-8') as contenu_fichier:
            historique = contenu_fichier.read().split('\n')

        # La raison pourquoi on ouvre pas le fichier en lecture + écriture c'est parce qu'on veut contrôler
        # le nombre maximum de parties enregistrer dans celui-ci. On doit donc d'abord lire le contenu du fichier,
        # ensuite enregistrer par dessus celui-ci en s'assurant que la limite n'est pas dépassé.
        fichier_historique = open('historique.txt', 'w', encoding='UTF-8')

        for i in range(len(historique)): historique[i] += '\n'

        # Maximum de 50 parties enregistrer.
        if len(historique) == 50: del historique[0]

        if gagnant[0] == 'blanc':
            nom_gagnant = self.joueur_blanc.nom_joueur
        else:
            nom_gagnant = self.joueur_noir.nom_joueur

        # nom du gagnant / couleur du gagnant / nombre de points du gagnant / JJ / MMMM / AAAAA
        historique.append(f'{nom_gagnant}/{gagnant[0]}/{gagnant[1]}/{now.day}/{now.month}/{now.year}')

        fichier_historique.writelines(historique)
        fichier_historique.close()


    def verifier_partie_en_cours(self):
        """
        Vérifie si il y a une partie en cours.
        """
        return (False, True)[path.exists(self.partie_en_cours)]

    def menu_douverture(self):
        """
        Cette méthode affiche un menu à l'ouverture de l'application avec différentes options pour l'utilisateur.
        - Nouvelle partie
        - Continuer la partie en cours
        - Règles du jeu (En construction)
        - Historique
        - Quitter
        """
        while True: # Boucle infini jusqu'à ce que l'utilisateur souhaite quitter l'application.

            print('\n{:->50}\nBienvenue sur Othello !'.format(''))
            menu = '(N)ouvelle partie\n'
            if self.verifier_partie_en_cours(): menu += '(C)ontinuer la partie en cours\n'
            menu += '(R)ègles du jeu (En construction)\n(H)istorique\n(Q)uitter\n'
            print('{0:->50}\n{1}{0:->50}'.format('', menu))
            commande = input('Votre commande : ')
            print('{:->50}\n'.format(''))

            # On s'assure que la commande entrer par l'utilisateur fait partie des options.
            # Si c'est le cas, on retourne ce qu'il a demandé.
            if commande == 'n' or commande == 'N':
                self.planche.initialiser_planche_par_default()
                self.initialiser_joueurs()
                self.jouer()
            elif (self.verifier_partie_en_cours()) and (commande == 'c' or commande == 'C'):
                self.charger(self.partie_en_cours)
                self.jouer()
            elif commande == 'r' or commande == 'R':
                print('En construction...')
            elif commande == 'h' or commande == 'H':
                while True:
                    nombre_parties = int(input('Entrer le nombre de partie à afficher: '))
                    if nombre_parties < 1 or nombre_parties > 50:
                        continue
                    else:
                        self.afficher_historique(nombre_parties)
                        break
            elif commande == 'q' or commande == 'Q':
                exit()
            # Sinon on affiche une erreur.
            else:
                print('\033[31mErreur : Entrée non valide.\033[30m')
