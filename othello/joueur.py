from random import choice

class Joueur:
    """
    Classe générale de joueur. Vous est fournie.
    """

    def __init__(self, couleur):
        """
        Le constructeur global de Joueur.

        Args:
            couleur: La couleur qui sera jouée par le joueur.
        """
        assert couleur in ["blanc", "noir"], "Piece: couleur invalide."

        self.couleur = couleur

    def obtenir_type_joueur(self):
        '''
        Cette méthode sera utilisée par les sous-classes JoueurHumain et JoueurOrdinateur.

        Returns:
            Le type de joueur, 'Ordinateur' ou 'Humain'
        '''
        pass

    def choisir_coup(self, coups_possibles):
        '''
        Cette méthode sera implémentée par les sous-classes JoueurHumain et JoueurOrdinateur.

        Args:
            coups_possibles: la liste des coups possibles

        Returns:
            un couple (ligne, colonne) représentant la positon du coup désiré.
        '''
        pass


class JoueurHumain(Joueur):
    """
    Classe modélisant un joueur humain.
    """

    def __init__(self, couleur, nom = ''):
        """
        Cette méthode va construire un objet Joueur et l'initialiser avec la bonne couleur.
        """
        super().__init__(couleur)

        # On crée une variable nom pour les joueurs humains.
        self.nom_joueur = nom

    def obtenir_type_joueur(self):
        return "Humain"

    def choisir_coup(self, coups_possibles):
        """
        Demande successivement à l'usager à quelle ligne, puis à quelle colonne il désire jouer.

        Nous vous avons déjà fourni une portion de code permettant d'attrapper au passage les erreurs qui peuvent
        survenir lorsque l'utilisateur entre autre chose qu'un nombre entier.

        Args:
            coups_possibles: La liste des coups possibles

        Returns:
            un couple (ligne, colonne) représentant la position du coup désiré.
        """
        try:
            # Ici on valide seulement si l'utilisateur entre un nombre entier.
            position_ligne = int(input('Quel est la position que vous désirez jouer pour votre coup. '
                                       '\033[04mLigne\033[30m : '))

            position_colonne = int(input(f'Quel est la position que vous désirez jouer pour votre coup. '
                                         f'\033[04mLigne\033[30m : {position_ligne} - \033[04mColonne\033[30m : '))

            return (position_ligne, position_colonne)

        except ValueError:
            print("Erreur : Position invalide.\n")
            print("")
            # L'usager a fait une erreur de saisie, on retourne donc un coup que l'on sait invalide.
            # Ceci forcera le programme à redemander le coup au joueur.
            return (-1, -1)


class JoueurOrdinateur(Joueur):
    """
    Classe modélisant un joueur Ordinateur.
    """
    def __init__(self, couleur, niveau = 'Facile'):
        """
        Cette méthode va construire un objet Joueur et l'initialiser avec la bonne couleur.
        """
        super().__init__(couleur)

        # Détermine le niveau de difficulté du JoueurOrdinateur.
        self.niveau = niveau

    def obtenir_type_joueur(self):
        return "Ordinateur"

    def choisir_coup(self, coups_possibles, planche = None):
        """
        Pour votre joueur ordinateur, vous n'avez qu'à sélectionner un coup au hasard parmi la liste des coups
        possibles. Affichez ensuite en console les numéros de ligne et de colonne.

        Pour faire un choix au hasard, vous devrez faire appel à la libraire random de Python. Explorez là, elle
        possède même une fonction retournant précisément un choix aléatoire parmi une liste.

        Args:
            coups_possibles: La liste des coups possibles
            planche: Instance de la planche pour les niveaux de difficultés autre que 'facile'

        Returns:
            un couple (ligne, colonne) représentant la position du coup désiré.
        """
        if self.niveau == 'Facile': return self.facile(coups_possibles, planche)
        elif self.niveau == 'Normal': return self.normal(coups_possibles, planche)
        else: print('Erreur dans le choix de difficulté pour le joueur ordinateur.')

    def facile(self, coups_possibles, planche):
        """
        Pour le mode facile on base simplement les choix de l'ordinateur sur du hasard.

        Args:
            coups_possibles: La liste des coups possibles
            planche: Instance de la planche

        Returns:
            un couple (ligne, colonne) représentant la position du coup désiré.
        """
        position_choisie = choice(coups_possibles)

        print(f'Le joueur de l\'équipe des \033[04m{self.couleur}s\033[30m a choisit de jouer le coup suivant : '
              f'\033[04mLigne\033[30m : {position_choisie[0]} - \033[04mColonne\033[30m : {position_choisie[1]}')

        return (position_choisie[0], position_choisie[1])

    def normal(self, coups_possibles, planche):
        """
        Ce modèle d'intelligence artificielle base sa logique sur 2 critères plutôt que purement du hasard.
        1. Joue le coup sur un coin de la planche si possible
        2. Joue le coup qui rapport le plus de points possible : si il y en a plusieurs, choisit au hasard.

        Args:
            coups_possibles: La liste des coups possibles
            planche: Instance de la planche

        Returns:
            un couple (ligne, colonne) représentant la position du coup désiré.
        """
        points_meilleur_coup, position_choisie = 0, ()

        for coup in coups_possibles:
            if (coup == (0, 0)) or (coup == (0, 7)) or (coup == (7, 0)) or (coup == (7, 7)):
                position_choisie = coup
                break
            else:
                positions_mangees = planche.obtenir_positions_mangees(coup, self.couleur)
                nombre_position_mangees = 0
                for direction in positions_mangees:
                    for position in direction:
                        if position != []: nombre_position_mangees += 1

                if nombre_position_mangees > points_meilleur_coup:
                    position_choisie, points_meilleur_coup = coup, nombre_position_mangees
                # Si le nombre de points est égal, on choisit au hasard quel coup prendre.
                elif nombre_position_mangees == points_meilleur_coup:
                    if choice([True, False]): position_choisie, points_meilleur_coup = coup, nombre_position_mangees

        print(f'Le joueur de l\'équipe des \033[04m{self.couleur}s\033[30m a choisit de jouer le coup suivant : '
              f'\033[04mLigne\033[30m : {position_choisie[0]} - \033[04mColonne\033[30m : {position_choisie[1]}')

        return (position_choisie[0], position_choisie[1])